FROM python:3.7-alpine

RUN pip install requests

COPY RequestUrl.py /

CMD ["python3", "RequestUrl.py"]